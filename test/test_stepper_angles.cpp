#include <AUnit.h>
#include <PositionalStepper.h>
#include <MotorCalibration.h>

using namespace aunit;

PositionalStepper stepper_ {2048, 2,3,4,5};


class TestOnceStepper : public TestOnce {
public:
    void teardown() override {
        stepper_.Reset();
    }
};

testF(TestOnceStepper, StepperCreatedWithZero) {
    assertEqual(stepper_.GetAngle(), 0);
}

testF(TestOnceStepper, AddAngleSumCorrect) {
    stepper_.AddAngle(90);
    assertEqual(stepper_.GetAngle(), 90);
    stepper_.AddAngle(60);
    assertEqual(stepper_.GetAngle(), 150);
    stepper_.AddAngle(-110);
    assertEqual(stepper_.GetAngle(), 40);
}

testF(TestOnceStepper, AddAngleWraps360) {
    stepper_.AddAngle(370);
    assertEqual(stepper_.GetAngle(), 10);
    stepper_.AddAngle(-30);
    assertEqual(stepper_.GetAngle(), 340);
    stepper_.AddAngle(20);
    assertEqual(stepper_.GetAngle(), 0);
}

testF(TestOnceStepper, AddNegativeAngle) {
    stepper_.AddAngle(-1);
    assertEqual(stepper_.GetAngle(), 359);
}

test(TestCalibrationFromZero) {
    MotorCalibration calibration;

    calibration.Start(0);
    auto coefficient = calibration.GetAngleCoefficient(180, 90);
    assertNear(coefficient, 0.5, 0.000001);
}

test(TestCalibrationFromNonZero) {
    MotorCalibration calibration;

    // rotate 90 deg from 10 deg
    calibration.Start(10);
    auto coefficient = calibration.GetAngleCoefficient(55, 100);
    assertNear(coefficient, 2., 0.000001);
}

void setup() {
    Serial.begin(115200);
    while (! Serial); // Wait until Serial is ready - Leonardo/Micro

    TestRunner::include("TestOnceStepper", "*");
    TestRunner::include("TestCalibration*");
}

void loop() {
    TestRunner::run();
}
