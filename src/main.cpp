#include <Arduino.h>

#include <PositionalStepper.h>
#include <LedController.h>

#include <Logger.h>
#include <CommandLine.h>

// azimuth axis inverted
PositionalStepper azimuth_motor{2038, 9, 8, 7, 6};
PositionalStepper altitude_motor{2038, 2, 3, 4, 5};

LedController status_led{LED_BUILTIN};

void addAzimuth(ArgumentParser &args) {
    azimuth_motor.AddAngle((int) args.ReadLong());
}

void addAltitude(ArgumentParser &args) {
    altitude_motor.AddAngle((int) args.ReadLong());
}

void sendStats(ArgumentParser &args) {
    auto pin = args.isFinished() ? 0 : args.ReadLong();
    auto pin_value = analogRead(pin);
    Logger::log()
        << azimuth_motor.GetAngle() << ','
        << altitude_motor.GetAngle() << ','
        << pin_value << '\n';
}

void waitMilliseconds(ArgumentParser &args) {
    delay(args.ReadLong());
}

void reset(ArgumentParser &args) {
    azimuth_motor.Reset();
    altitude_motor.Reset();
}

CommandLine cmd{
    new Command[5]{
        {"az", addAzimuth},
        {"al", addAltitude},
        {"st", sendStats},
        {"w", waitMilliseconds},
        {"r", reset},
    },
    5
};

void setup() {
    Logger::Start();
    status_led.init();
    azimuth_motor.SetSpeed(5);
    altitude_motor.SetSpeed(5);
}

void loop() {
    cmd.RunCommandIfPresent();
}
