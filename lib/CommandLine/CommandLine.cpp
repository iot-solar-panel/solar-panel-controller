#include "CommandLine.h"
#include "ArgumentParser.h"

#include <Arduino.h>
#include <Logger.h>

#include <string.h>
#include <stdlib.h>

CommandLine::CommandLine(Command *commands, int command_amount)
    : commands_(commands), command_amount_(command_amount) {
}

bool CommandLine::PollCommand() {
    /*
     * returns true if successfully received new command from serial port
     * false otherwise
     */
    static uint8_t chars_read = 0;
    // read asynchronously until full command input
    while (Serial.available()) {
        char c = static_cast<char>(Serial.read());
        if (c == '\n') {
            text_buffer_[chars_read] = '\0';
            chars_read = 0;
            return false;
        } else {
            if (chars_read < BUFFER_SIZE) {
                text_buffer_[chars_read++] = c;
            }
            text_buffer_[chars_read] = '\0';
        }
    }
    return true;
}

void CommandLine::RunCommandIfPresent() {
    if (PollCommand()) return;

    String cmd {text_buffer_};
    int space_index = cmd.indexOf(' ');

    String command_name;
    String args;
    if (space_index != -1) {
        command_name = cmd.substring(0, space_index);
        args = cmd.substring(space_index + 1);
    } else {
        command_name = cmd.substring(0);
        args = "";
    }

    const Command *command = FindCommand(command_name);
    if (command != nullptr) {
        command->Run(args);
    }
}

Command *CommandLine::FindCommand(const String& command_name) {
    for (size_t i = 0; i < command_amount_; i++) {
        if (commands_[i].GetName() == command_name) {
            return &commands_[i];
        }
    }
    return nullptr;
}
