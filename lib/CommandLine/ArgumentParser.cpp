#include "ArgumentParser.h"

ArgumentParser::ArgumentParser(String &args) : position_(0), args_(args) {
}

long ArgumentParser::ReadLong() {
    if (not isFinished()) {
        return ReadString().toInt();
    } else {
        return 0;
    }
}

String ArgumentParser::ReadString() {
    auto start = position_;

    int coma = args_.indexOf(',', start);
    unsigned long end;

    if (coma == -1) {
        end = args_.length();
    } else {
        end = coma;
    }

    position_ = end + 1;

    return args_.substring(start, end);
}

bool ArgumentParser::isFinished() const {
    return position_ >= args_.length();
}
