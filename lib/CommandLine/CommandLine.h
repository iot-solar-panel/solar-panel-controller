#pragma once

#include <WString.h>

#include "Command.h"

class CommandLine {
public:
    static const int BUFFER_SIZE = 30;

    explicit CommandLine(Command *commands, int command_amount);

    void RunCommandIfPresent();

private:
    char text_buffer_[BUFFER_SIZE] {0};
    Command *commands_;
    const int command_amount_;

    bool PollCommand();
    Command *FindCommand(const String& command_name);
};
