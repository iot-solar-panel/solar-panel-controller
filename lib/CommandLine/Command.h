#pragma once

#include <WString.h>

#include "ArgumentParser.h"

class Command {
public:
    Command(const String& name, void (*action)(ArgumentParser &));

    const String &GetName() const;

    void Run(String &args) const;

private:
    String name_;
    void (*action_)(ArgumentParser &);
};


