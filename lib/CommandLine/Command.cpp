#include "Command.h"
#include "ArgumentParser.h"

Command::Command(const String &name, void (*action)(ArgumentParser &))
    : name_(name), action_(action) {

}

const String &Command::GetName() const {
    return name_;
}

void Command::Run(String &args) const {
    ArgumentParser parser {args};
    action_(parser);
}
