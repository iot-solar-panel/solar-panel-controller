#pragma once

#include <WString.h>

class ArgumentParser {
public:
    explicit ArgumentParser(String &args);

    bool isFinished() const;
    long ReadLong();
    String ReadString();

private:
    unsigned int position_;
    String args_;
};


