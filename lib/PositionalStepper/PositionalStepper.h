#pragma once

#include <Arduino.h>
#include <Stepper.h>

class PositionalStepper {
public:
    PositionalStepper(int steps_per_revolution, int pin1, int pin2, int pin3, int pin4);

    void Reset();
    void AddAngle(int increment);
    int GetAngle() const;
    void SetSpeed(int speed);

private:
    float current_position_ {0};
    float step_per_degree_;
    Stepper stepper_;

    static constexpr float FULL_ROTATION_ANGLE = 360;

    inline int ToSteps(int angle) const;

    /*
     * Converts angle to interval [0, 359]
     */
    static float ClampAngle(float angle);
};


