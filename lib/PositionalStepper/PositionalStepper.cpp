#include <PositionalStepper.h>

#include <Logger.h>

PositionalStepper::PositionalStepper(int steps_per_revolution, int pin1, int pin2, int pin3, int pin4)
    : step_per_degree_(static_cast<float>(steps_per_revolution) / FULL_ROTATION_ANGLE),
    // pin3 and pin2 must be swapped to fix backwards movement
      stepper_(steps_per_revolution, pin1, pin3, pin2, pin4) {
}

void PositionalStepper::AddAngle(int increment) {
    current_position_ = ClampAngle(current_position_ + increment);
    stepper_.step(ToSteps(increment));
}

int PositionalStepper::ToSteps(int angle) const {
    return static_cast<int>(angle * step_per_degree_);
}

void PositionalStepper::SetSpeed(int speed) {
    stepper_.setSpeed(speed);
}

int PositionalStepper::GetAngle() const {
    return static_cast<int>(current_position_);
}

void PositionalStepper::Reset() {
    current_position_ = 0;
}

float PositionalStepper::ClampAngle(float angle) {
    while (angle >= FULL_ROTATION_ANGLE) {
        angle -= FULL_ROTATION_ANGLE;
    }
    while (angle < 0) {
        angle += FULL_ROTATION_ANGLE;
    }
    return angle;
}
