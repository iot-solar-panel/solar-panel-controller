#pragma once

#include <Arduino.h>


class Logger {
public:
    static Logger& log() {
        if (instance_ == nullptr) {
            instance_ = new Logger();
        }
        return *instance_;
    }

    Logger& operator<<(String& message);
    Logger& operator<<(float message);
    Logger& operator<<(int message);
    Logger& operator<<(long message);
    Logger& operator<<(unsigned long message);

    template<typename MessageType>
    Logger& operator<<(MessageType message);

    static void Start();

private:
    static Logger* instance_;

    Logger() = default;
    ~Logger();
};


template<typename MessageType>
Logger &Logger::operator<<(MessageType message) {
    Serial.write(message);
    return *this;
}
