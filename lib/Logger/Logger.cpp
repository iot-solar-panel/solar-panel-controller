#include "Logger.h"

Logger* Logger::instance_ = nullptr;


Logger::~Logger() {
    delete instance_;
}

void Logger::Start() {
    Serial.begin(9600);
}

Logger &Logger::operator<<(float message) {
    Serial.write(String(message).c_str());
    return *this;
}

Logger &Logger::operator<<(int message) {
    Serial.write(String(message, 10).c_str());
    return *this;
}

Logger &Logger::operator<<(long message) {
    Serial.write(String(message, 10).c_str());
    return *this;
}

Logger &Logger::operator<<(unsigned long message) {
    Serial.write(String(message, 10).c_str());
    return *this;
}

Logger& Logger::operator<<(String& message) {
    Serial.write(message.c_str());
    return *this;
}

